package terbilang

import (
	"math"
	"strings"
)

var (
	digits = [...]string{"", "satu ", "dua ", "tiga ", "empat ", "lima ", "enam ", "tujuh ", "delapan ", "sembilan "}
	group  = [...]string{"ribu ", "juta ", "milyar ", "trilyun "}
)

func Int(num int64) string {
	if num > 999999999999999 {
		return ""
	}
	if num == 0 {
		return "nol "
	}
	var s [31]string
	if num < 0 {
		s[0] = "minus "
		num = -num
	}
	i := 0
	j := 30
	rem := num % 1000
	for {
		if j == 24 && rem == 1 {
			s[j] = "se"
		} else {
			y := rem / 100
			if y == 1 {
				s[j-3] = "seratus "
			} else if y != 0 {
				s[j-4] = digits[y]
				s[j-3] = "ratus "
			}
			x := rem % 100
			if x == 10 {
				s[j] = "sepuluh "
			} else if x == 11 {
				s[j] = "sebelas "
			} else if x > 11 && x < 20 {
				s[j-1] = digits[x%10]
				s[j] = "belas "
			} else if x != 0 {
				if x > 19 {
					s[j-2] = digits[x/10]
					s[j-1] = "puluh "
				}
				s[j] = digits[x%10]
			}
		}
		num /= 1000
		if num != 0 {
			rem = num % 1000
			if rem != 0 {
				s[j-5] = group[i]
			}
			i++
		} else {
			break
		}
		j -= 6
	}
	return strings.Join(s[:], "")
}
func IntOrFloat(x any) string {
	switch y := x.(type) {
	case int64:
		return Int(y)
	case float64:
		a, b := fract(y)
		return strings.Join([]string{Int(a), "koma ", Int(b)}, "")
	}
	return ""
}
func fract(x float64) (int64, int64) {
	a, b := math.Modf(x)
	for {
		b *= 10
		if _, c := math.Modf(b); c == 0 {
			break
		}
	}
	return int64(a), int64(b)
}
