package main

import (
	"fmt"

	"gitlab.com/birowo/terbilang"
)

func main() {
	nums := []int64{
		-123456789123456, 654321987654321, 115114113112111, 111111111111111,
		101010101010101, 110101010101010, 101001001001000, -101000, -10000,
		1000, 2211, 212, 1000000000, -11, 11000, -12, 1000001011, 111, 110,
	}
	for _, num := range nums {
		println(num, ":", terbilang.Int(num))
	}
	for _, num := range []any{int64(332211), float64(0.125)} {
		fmt.Printf("%T(%v) : %s\n", num, num, terbilang.IntOrFloat(num))
	}
}
